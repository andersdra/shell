alias aliases='vi ~/.bash_aliases'
alias ...='cd ../..'
alias ..='cd ..'
alias dds='docker rm $(docker ps -a -q)'
alias dim='docker images'
alias dps='docker ps'
alias drm='docker rm'
alias drmi='docker rmi'
alias dsp='docker system prune'
alias dstopall='docker stop $(docker ps -q)'
alias dtag='docker tag'
alias dut='docker rmi $(docker images -q -f dangling=true)'
alias ga='git add'
alias gd='git diff'
alias gitad='git add --update'
alias gits='git status'
alias hadolint='docker run --rm -i hadolint/hadolint < Dockerfile'
alias l='ls'
alias ls='ls --color=auto'
alias manage_cups='xdg-open http://localhost:631'
alias mplab_ide='docker start mplab_ide'
alias vd='vi Dockerfile'
alias n='nano'
alias nano='vi'
alias v='vi'
alias resource='. ~/.bashrc'
alias sc='shellcheck'
alias synctunnel='ssh -f 10.0.22.49 -L 8333:localhost:8384 -N'
alias tor='docker run \
 --rm \
 --name tor_firefox \
 --cap-drop ALL \
 -e DISPLAY \
 -v $HOME/.Xauthority:/home/container/.Xauthority:ro \
 -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
tor_browser'
alias webserver='docker run --rm -p 8080:80 -v "$PWD/src":/var/www/html php:7.3-apache-stretch'

