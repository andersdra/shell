#!/usr/bin/env python3
# Serve HTML page with self signed certificate \
# that accesses KeePassXC through browser extension?
from http.server import HTTPServer, SimpleHTTPRequestHandler
import ssl

class HandleRequests(SimpleHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self._set_headers()
        self.wfile.write('get'.encode())

    def do_POST(self):
        self._set_headers()
        content_len = int(self.headers.get('content-length', 0))
        post_body = self.rfile.read(content_len)
        print("post: {}".format(post_body.decode()))

    def do_PUT(self):
        self.do_POST()

    def http_print(self, input, newline=1):
        self.wfile.write(b'<font color="white">' + input + b'</font>')
        if newline:
            self.wfile.write(b'<br/>')

    def http_println(self, input):
        self.wfile.write(input + b'\n')


httpd = HTTPServer(('127.0.0.1', 8888), HandleRequests)
httpd.socket = ssl.wrap_socket (httpd.socket, certfile='example.crt', keyfile='example.key', server_side=True)
httpd.serve_forever()
