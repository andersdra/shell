import sys
import socket

ssh_key_id=0

if len(sys.argv) > 2:
    key_password=sys.argv[1]
    ssh_key_id=sys.argv[2]
else:
    if len(sys.argv) < 2:
        print('No password input for key 0')
        exit(1)
    key_password=sys.argv[1]

hostname=socket.gethostname()

if not hasattr(sys, 'real_prefix'):
    print('Not running in virtual env')
    exit(1)

from keepassxc_browser import Connection, Identity, ProtocolError
from pathlib import Path

client_id = 'python-keepassxc-browser'

state_file = Path('.assoc')
if state_file.exists():
    with state_file.open('r') as f:
        data = f.read()
    id = Identity.unserialize(client_id, data)
else:
    id = Identity(client_id)

c = Connection()
c.connect()
c.change_public_keys(id)
try:
    c.get_database_hash(id)
except ProtocolError as ex:
    print(ex)
    exit(1)

if not c.test_associate(id):
    associated_name = c.associate(id)
    assert c.test_associate(id)
    data = id.serialize()
    with state_file.open('w') as f:
        f.write(data)
    del data

# add key password to database
c.set_login(id, \
url='https://ssh-keys:{}:{}'.format(hostname, ssh_key_id), \
login="{}:{}".format(hostname, ssh_key_id), \
password=key_password, \
entry_id=None, \
submit_url=None)

c.disconnect()
