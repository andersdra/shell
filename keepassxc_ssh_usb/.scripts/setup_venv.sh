#!/bin/sh

virtualenv ../env
. ../env/bin/activate
pip3 install git+https://github.com/hrehfeld/python-keepassxc-browser.git
