#!/bin/bash
# create SSH keys for USB storage

if [ -z "$1" ]
then
  echo "./$0 passes key_comment"
  exit 1
fi

if [ -z "$SSH_ADD_KEY_ID" ]
then
  SSH_ADD_KEY_ID=0
fi

if [ -z "$HOSTNAME" ]
then
  HOSTNAME=$(uname -n)
fi

ssh_host_dir="../.hosts/.$HOSTNAME"

if [ ! -d "$ssh_host_dir" ]
then
  mkdir --parents "$ssh_host_dir"
fi
chmod 600 "$ssh_host_dir"

mkdir --parents "$ssh_host_dir/.$SSH_ADD_KEY_ID/.ssh"
chmod --recursive 700 "$ssh_host_dir/.$SSH_ADD_KEY_ID"

ssh-keygen -a "$1" -t ed25519 -f "$ssh_host_dir/.$SSH_ADD_KEY_ID/.ssh/id_ed25519" -C "$2" -V "-1m:forever"

chmod 700 "$ssh_host_dir/.$SSH_ADD_KEY_ID/.ssh/id_ed25519"
chmod 744 "$ssh_host_dir/.$SSH_ADD_KEY_ID/.ssh/id_ed25519.pub"
