#!/bin/sh
# page range extraction for pdf files
# useful for register summary's from Atmel/Microchip datasheets etc
#
# example file:
# page_range file_name
# 3-7 table_of_content
# 20-32 interesting_stuff

while read -r line
do
  pages=$(echo "$line" | awk '{print $1}')
  name=$(echo "$line" | awk '{print $2}')
  qpdf --pages "$2" "$pages" -- "$2" "$name".pdf
done < "$1"
