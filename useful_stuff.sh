#!/bin/bash
# various one-liners and functions

# get man page, format, send to printer
print_man(){
echo "Starting printing of 'man $1'"
/usr/bin/man "$1" | fmt -w 95 | lpr -o prettyprint
}

highlight_source(){
if [ -z "$1" ]
then
  echo 'GNU source-highlight'
  echo "Usage: hightlight_source 'output_format' 'input_file' 'output_file'"
  echo 'Formats: docbook, html, htmltable, latex, odf, xhtml'
  echo 'xhtmltable, esc, html-css, javadoc, latexcolor, texinfo, xhtml-css'
  return 1
fi
/usr/bin/source-highlight -f "$1" -i "$2" -o "$3" -n
}

tab_to_space(){
echo 'tab_to_space input_file output_file'
/usr/bin/expand -4 "$1" > "$2"
}

# use print_function print_function until remembered..
print_function(){
declare -f "$1"
}

function_helper(){
if [ -z "$1" ]
then echo "$2"; return 1
else return 0
fi
}

confirm_yes(){
echo "$1"
read -r confirmation \
&& if [ "$confirmation" = 'Y' ]
then return 0
else return 1
fi
}

tool_rsync_run(){
rsync_command='/usr/bin/rsync'
for arg in "$@"
do
  rsync_command+=" $arg"
done
if confirm_yes "Do --dry-run of '$rsync_command' ? Y/n"
then
  eval "$(sed 's/\<rsync\>/& --dry-run/' <<< "$rsync_command")" \
  && \
  if confirm_yes "Run '$rsync_command' ? Y/n"
  then eval "$rsync_command"
  else return 1
  fi
else
  if confirm_yes "Run '$rsync_command' ? Y/n"
  then eval "$rsync_command"
  else return 1
  fi
fi
}

rsync_file_to_local_ssh(){
function_helper "$1" 'rsync_file_to_local_ssh port user@ip /remote/file /to/path/' \
&& \
tool_rsync_run --compress --verbose --progress --human-readable --rsh="'ssh -p'$1" "$2:$3" "$4"
}

rsync_file_to_remote_ssh(){
function_helper "$1" 'rsync_file_to_remote_ssh port local_file user@ip dest_dir' \
&& \
tool_rsync_run --compress --verbose --progress --human-readable --rsh="'ssh -p'$1" "$2" "$3:$4"
}

rsync_folder_to_remote_ssh(){
function_helper "$1" 'rsync_folder_to_remote_ssh port local_file user@ip dest_dir' \
&& \
tool_rsync_run --compress --verbose --progress --human-readable --archive --rsh="'ssh -p'$1" "$2:$3" "$4"
}

rsync_folder_to_local_ssh(){
function_helper "$1" 'rsync_folder_to_local_ssh port user@ip /remote/path /to/local' \
&& \
tool_rsync_run --compress --verbose --progress --human-readable --archive --rsh="'ssh -p'$1" "$2:$3" "$4"
}

rsync_folder_to_remote(){
function_helper "$1" 'rsync_local_to_remote local_folder user:ip /remote/path' \
&& \
tool_rsync_run --compress --verbose --progress --human-readable --archive "$1" "$2:$3"
}

rsync_folder_to_local(){
function_helper "$1" 'rsync_folder_to_local user@ip:port remote_folder local_folder' \
&& \
tool_rsync_run --compress --verbose --progress --human-readable --archive "$1:$2" "$3"
}

rsync_file_local(){
function_helper "$1" 'rsync_file_local /path/to/file /to/path' \
&& \
tool_rsync_run --compress --verbose --progress --human-readable "$1" "$2"
}

rsync_folder_local(){
function_helper "$1" 'rsync_folder_local /from/dir /to/path' \
&& \
tool_rsync_run --compress --verbose --progress --human-readable --archive "$1" "$2"
}

