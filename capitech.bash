#!/bin/bash
# calculate PNS salary through Capitech

capitech_user=''
capitech_password=''
tax_percentage=''
base_salary_hour=''

declare -a salaries
salaries=( "$base_salary_hour" "$(bc -l <<< "$base_salary_hour * 1.5")" "$(bc -l <<< "$base_salary_hour * 2")" )
from_year="$(date +'%Y')"
current_year="$(date +'%Y')"
current_day="$(date +'%0d')"
current_month="$(date +'%0m')"
day_of_week="$(date +'%u')"
next_day="$(bc <<< "$current_day + 1")"

if [ "${day_of_week#0}" = '1' ];then # remove prepended zero of day_of_week
  yesterday="$(("${current_day#0} - 3"))"
else
  yesterday="$(("${current_day#0} - 1"))"
fi

if [ -f "$next_day" ];then
  rm "$next_day"
fi

if [ "$current_day" -gt 24 ];then
  last_month="$current_month"
else
  if [ "$current_month" -lt 2 ];then
    last_month=12
    from_year="$(bc <<< "$current_year - 1")"
  else
    last_month="$(bc <<< "$(date +%m) - 1")"
    if [ "$last_month" -lt 10 ];then
      last_month="0$last_month"
    fi
  fi
fi

access_token=''
refresh_token=''

capitech_authorize()
{
authorized="$(curl 'https://flow.capitech.no/PNS/api/public/v1/Webtid/authorize' \
-H 'Sec-Fetch-Mode: cors' \
-H 'Sec-Fetch-Site: same-origin' \
-H 'Origin: https://flow.capitech.no' \
-H 'Accept-Encoding: gzip, deflate, br' \
-H 'Accept-Language: en-US,en;q=0.9' \
-H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36' \
-H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' \
-H 'Accept: application/json, text/javascript, */*; q=0.01' \
-H 'Referer: https://flow.capitech.no/PNS/apps/MinCapitech/index.htm?v=15.9.0.337' \
-H 'X-Requested-With: XMLHttpRequest' \
-H 'Cookie: my-capitech-remember-me=""; my-capitech-clientid=100' \
--data "clientId=100&username=$capitech_user&password=$capitech_password" \
--compressed 2> /dev/null | jq '.content[0]')"

refresh_token="$(jq '.refreshToken' <<< "$authorized")"
access_token="$(jq '.accessToken' <<< "$authorized")"
}

get_hours()
{
no_quote_access_token="$(tr -d '"' <<< "$access_token")"
curl 'https://flow.capitech.no/PNS/api/public/v1/Webtid/getAccumulatedTime' \
-H 'Sec-Fetch-Mode: cors' \
-H 'Sec-Fetch-Site: same-origin' \
-H 'Origin: https://flow.capitech.no' \
-H 'Accept-Encoding: gzip, deflate, br' \
-H 'Accept-Language: en-US,en;q=0.9' \
-H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36' \
-H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' \
-H 'Accept: application/json, text/javascript, */*; q=0.01' \
-H 'Referer: https://flow.capitech.no/PNS/apps/MinCapitech/dashboard.htm?v=15.9.0.337' \
-H 'X-Requested-With: XMLHttpRequest' \
-H "Cookie: my-capitech-remember-me=\"\"; my-capitech-clientid=100; my-capitech-refresh-token=$refresh_token; my-capitech-access-token=$access_token" \
--data "accessToken=$no_quote_access_token&fromDate=$from_year-$last_month-25&toDate=$current_year-$current_month-$current_day" \
--compressed 2> /dev/null
}

calculate_salary()
{
hours="$(get_hours | jq '.content')"
if [ ! -f "$current_day" ];then
  if [ -f "$yesterday" ];then
    readarray -t yesterday < "$yesterday"
    printf '' > 'last_diff'
  fi
else
  if [ -f 'last_diff' ];then
    cat 'last_diff'
    printf '\n'
  fi
  printf '' > "$current_day"
fi

earned_today=0
earned_total=0
for i in 0 1 2
do
  desc="$(jq ".[$i].timeCategoryDescription" <<< "$hours" | tr -d '"')"
  amount="$(jq ".[$i].amount" <<< "$hours")"
  if [ ! "$desc" = 'null' ];then
    printf "%s: %s\n" "$desc" "$amount"
    earned_total="$(bc -l <<< "scale=2;$earned_total + ($amount * ${salaries[i]})")"
    if [ "${#yesterday[@]}" -gt 1 ];then
      if [ ! "$amount" = "${yesterday[i]}" ];then
        diff="$(bc -l <<< "scale=2;$amount - ${yesterday[i]}")"
        earned="$(bc -l <<< "scale=2;$diff * ${salaries[i]}")"
        earned_today="$(bc <<< "scale=2;$earned_today + $earned")"
        printf "+%s timer\n%s kr\n" "$diff" "$earned"
        printf "%s: %s\n" "$desc" "$diff" >> 'last_diff'
      fi
    fi
    echo "$amount" >> "$current_day"
  fi
done

if [ -v yesterday ];then
  tax="$(bc -l <<< "scale=2;(($earned_total * $tax_percentage) / 100)")"
  printf "\n%s,-\n" "$earned_today"
  echo "Brutto: $earned_total"
  echo "Netto: $(bc -l <<< "scale=2;$earned_total - $tax")"
fi
}

capitech_authorize
sleep .1
calculate_salary

